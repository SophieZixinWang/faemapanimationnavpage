//
//  ViewController.swift
//  AnimationPage
//
//  Created by Zixin on 1/22/17.
//  Copyright © 2017 Zixin. All rights reserved.
//

import UIKit

class ViewController: UIViewController{
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    var uiviewNavBar: UIView!
    var btnNavBar: UIButton!
    
    var pinbtn: UIButton!
    var placebtn: UIButton!
    var locabtn: UIButton!
    
    var backgroundpicView: UIImageView!
    var backgroundtxtView: UIImageView!
    var btnBack: UIButton!
    
    var NavBarLine1: UIImageView!
    var NavBarLine2: UIImageView!
    
    var btnUp: UIButton!
    var btnDown: UIButton!
    
    var uiviewNavBarMenu: UIView!
    var u = true;
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadNavBar()
    }
    
    func loadNavBar() {
        
        loadNavBarMenu()
        
        uiviewNavBar = UIView(frame: CGRect(x: -1, y: -1, width: screenWidth+2, height: 66))
        uiviewNavBar.layer.borderColor = UIColor(red: 200/255, green: 199/255, blue: 204/255, alpha: 1).cgColor
        uiviewNavBar.layer.borderWidth = 1
        uiviewNavBar.backgroundColor = UIColor.white
        
        backgroundpicView = UIImageView(frame: CGRect(x: 140, y: 365, width: 100, height: 98))
        backgroundpicView.image = #imageLiteral(resourceName: "Group 3")
        
        backgroundtxtView = UIImageView(frame: CGRect(x: 82, y: 254, width: 252, height: 103))
        backgroundtxtView.image = #imageLiteral(resourceName: "Group 4")
            
    
        self.view.addSubview(backgroundtxtView)
        self.view.addSubview(backgroundpicView)
        self.view.addSubview(uiviewNavBar)
        
        btnNavBar = UIButton(frame: CGRect(x: 0, y: 29, width: 200, height: 27))
        btnNavBar.center.x = screenWidth / 2
        btnNavBar.setTitle("My Pins", for: .normal)
        btnNavBar.setTitleColor(UIColor.gray, for: .normal)
        btnNavBar.titleLabel?.font = UIFont(name: "AvenirNext-Medium", size: 20)
        btnNavBar.titleLabel?.textAlignment = .center
        
        btnBack = UIButton(frame: CGRect(x: 0, y: 30, width: 50, height: 30))
        btnBack.setImage(#imageLiteral(resourceName: "Path 851"), for: UIControlState.normal)
        
        btnDown = UIButton(frame: CGRect(x: 252, y: 38, width: 15, height: 10))
        btnDown.setImage(#imageLiteral(resourceName: "Path 851Down"), for: UIControlState.normal)
        
        
        
        
        
        btnDown.addTarget(self, action: #selector(self.actionNavMenu(_:)),for: .touchUpInside)
        btnNavBar.addTarget(self, action: #selector(self.actionNavMenu(_:)), for: .touchUpInside)
        btnBack.addTarget(self, action: #selector(self.closeit(_:)), for: .touchUpInside)
        
        uiviewNavBar.addSubview(btnDown)
        uiviewNavBar.addSubview(btnBack)
        uiviewNavBar.addSubview(btnNavBar)
    }
    
    func closeit(_ sender: UIButton){
        dismiss(animated: true, completion: nil)
    }
    
    private func loadNavBarMenu() {
        uiviewNavBarMenu = UIView(frame: CGRect(x: -1, y: 64, width: screenWidth+2, height: 158))
        uiviewNavBarMenu.center.y -= 158
        uiviewNavBarMenu.layer.borderColor = UIColor(red: 200/255, green: 199/255, blue: 204/255, alpha: 1).cgColor
        uiviewNavBarMenu.layer.borderWidth = 1
        uiviewNavBarMenu.backgroundColor = UIColor.white
        
        
        pinbtn = UIButton(frame: CGRect(x: 10, y: 15, width: screenWidth - 20, height: 25))
        pinbtn.center.x = screenWidth / 2
        pinbtn.setTitle("Saved Pins", for: .normal)
        pinbtn.setTitleColor(UIColor.gray, for: .normal)
        pinbtn.titleLabel?.font = UIFont(name: "AvenirNext-Medium", size: 20)
        pinbtn.titleLabel?.textAlignment = .center
        
        pinbtn.addTarget(self, action: #selector(self.actionNavMenu(_:)), for: .touchUpInside)
        uiviewNavBarMenu.addSubview(pinbtn)
        
        placebtn = UIButton(frame: CGRect(x: 10, y: 65, width: screenWidth - 20, height: 25))
        placebtn.center.x = screenWidth / 2
        placebtn.setTitle("Saved Places", for: .normal)
        placebtn.setTitleColor(UIColor.gray, for: .normal)
        placebtn.titleLabel?.font = UIFont(name: "AvenirNext-Medium", size: 20)
        placebtn.titleLabel?.textAlignment = .center
        
        placebtn.addTarget(self, action: #selector(self.actionNavMenu(_:)), for: .touchUpInside)
        uiviewNavBarMenu.addSubview(placebtn)
        
        locabtn = UIButton(frame: CGRect(x: 10, y: 115, width: screenWidth - 20, height: 25))
        locabtn.center.x = screenWidth / 2
        locabtn.setTitle("Saved Locations", for: .normal)
        locabtn.setTitleColor(UIColor.gray, for: .normal)
        locabtn.titleLabel?.font = UIFont(name: "AvenirNext-Medium", size: 20)
        locabtn.titleLabel?.textAlignment = .center
        
        locabtn.addTarget(self, action: #selector(self.actionNavMenu(_:)), for: .touchUpInside)
        uiviewNavBarMenu.addSubview(locabtn)
        
        

        NavBarLine1 = UIImageView(frame: CGRect(x: 205, y: 55, width: 282.36, height: 1))
        NavBarLine1.center.x = screenWidth / 2
        NavBarLine1.image = #imageLiteral(resourceName: "Line1")
        
        uiviewNavBarMenu.addSubview(NavBarLine1)
        
        NavBarLine2 = UIImageView(frame: CGRect(x: 205, y: 105, width: 282.36, height: 1))
        NavBarLine2.center.x = screenWidth / 2
        NavBarLine2.image = #imageLiteral(resourceName: "Line1")
        
        uiviewNavBarMenu.addSubview(NavBarLine2)
        
    
        self.view.addSubview(uiviewNavBarMenu)
    }
    
    
    
    func actionNavMenu(_ sender: UIButton) {
        if(u)
        {
            UIView.animate(withDuration: 0.583,animations: {
                self.uiviewNavBarMenu.center.y += 158
            })
            btnDown.setImage(#imageLiteral(resourceName: "Path 851Up"), for: UIControlState.normal)
            u = false;
        }
        else
        {
            UIView.animate(withDuration: 0.583,animations: {
                  self.uiviewNavBarMenu.center.y -= 158
                })
            btnDown.setImage(#imageLiteral(resourceName: "Path 851Down"), for: UIControlState.normal)
            u = true;
        }
        
    }
   
   

    
    
}

